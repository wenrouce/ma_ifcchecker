# Handover Description 
The handover is divided into five sections:
1. The models (including the final version of revit and ifc)
2. Vortrag_powerpoint slides
3. Masterthesis_schriftfassung in pdf version
4. IFC Material Enrichment Tool (including pyrevit plugin and write to ifc python script)
5. IFC Validator Tool (including python script and flask app)

    _For specific descriptions of each section, please click on the corresponding folder to view_

