# -*- coding:utf-8 -*-
import os
from flask import Flask, render_template, request

from ifc_parse import *
from property_list import property_list


app = Flask(__name__)
"""first web page shows the final score the scores of each element type such as outerwall, innerwall, ..., material"""
@app.route('/')
def table():
    tup_data = []
    for key in element_weight.keys():
        list_key = [key.lower()]
        va = element_weight.get(key)
        for var in va:
            list_key.append(var)
        tup_data.append(tuple(list_key))

    entities = ["Roof", "Roof_ifcSlab"]
    for key in element_weight:
        entities.append(key.lower())

    headings = ('Entity', 'Weight', 'Score')
    data = tuple(tup_data)
    title = 'Quality Report'
    return render_template('table.html', title=title, headings=headings, data=data, score=final_score,
                           entities=entities)

"""Get the detailed property values of outerwall"""
@app.route('/OuterWall/')
def outerwall():
    """get the peoperty name, weight, and score of each property from the template"""
    name = OuterWall.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(OuterWall.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in OuterWall.element_dict:
            list_key = [key, OuterWall.element_dict[key]['weight'], OuterWall.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))

        """get the detailed data of every element"""
        properties = ['elementname']
        for key in OuterWall.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(OuterWall.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in OuterWall.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(OuterWall.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)

        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in OuterWall.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'outerwall_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=OuterWall.c[0],
                               ge_headings=ge_headings, ge_data=ge, properties=properties,
                               property_list=property_list, elementlist=elementlist, reference=reference
                               )

"""The table shows the instance list of outerwall which have the missing value of a certain property"""
@app.route('/OuterWall/<string:prop_na>')
def outerwall_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(OuterWall.c[1][prop_na]['elementlists'])
        """key elementlists--> value: displays the elements name which don't consist of the checked property"""
        reference = 'outerwall_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = OuterWall.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)

"""The table shows the detailed material construction, including material name and thickness"""

@app.route('/OuterWall/Materiallayerconstruction/<string:ma>')
def outerwall_layer_structure(ma):
    headings = ('layer_id', 'material name', 'thickness[m]')
    data_info = OuterWall.c[3][ma]
    if isinstance(data_info, str):
        data= 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name'], value['thickness']))
        data = tuple(data)
        return render_template('materiallayer.html', data=data, headings=headings)


@app.route('/InnerWall/')
def innerwall():
    name = InnerWall.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(InnerWall.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in InnerWall.element_dict:
            list_key = [key, InnerWall.element_dict[key]['weight'], InnerWall.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in InnerWall.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(InnerWall.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in InnerWall.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(InnerWall.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)

        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in InnerWall.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'innerwall_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=InnerWall.c[0],
                               ge_headings=ge_headings, ge_data=ge,
                               properties=properties, property_list=property_list, elementlist=elementlist,
                               reference=reference
                               )


@app.route('/InnerWall/<string:prop_na>')
def innerwall_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(InnerWall.c[1][prop_na]['elementlists'])
        reference = 'innerwall_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = list(InnerWall.c[3].keys())
            ma_list = tuple(ma_list)

            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)


@app.route('/InnerWall/MaterialLayer/<string:ma>')
def innerwall_layer_structure(ma):
    headings = ('layer_id', 'material name', 'thickness[m]')
    data_info = InnerWall.c[3][ma]
    if isinstance(data_info, str):
        data = 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name'], value['thickness']))
    data = tuple(data)
    return render_template('materiallayer.html', data=data, headings=headings)


@app.route('/Roof/')
def roof():
    name = Roof.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Roof.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Roof.element_dict:
            list_key = [key, Roof.element_dict[key]['weight'], Roof.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in Roof.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Roof.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Roof.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(Roof.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)

        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in Roof.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'roof_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Roof.c[0],
                               ge_headings=ge_headings, ge_data=ge, properties=properties,
                               property_list=property_list, elementlist=elementlist, reference=reference
                               )


@app.route('/Roof/<string:prop_na>')
def roof_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(Roof.c[1][prop_na]['elementlists'])
        reference = 'roof_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = Roof.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)


@app.route('/Roof/MaterialLayer/<string:ma>')
def roof_layer_structure(ma):
    headings = ('layer_id', 'material name', 'thickness[m]')
    data_info = Roof.c[3][ma]
    if isinstance(data_info, str):
        data = 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name']))
    data = tuple(data)
    return render_template('materiallayer.html', data=data, headings=headings)



@app.route('/Floor/')
def floor():
    name = Floor.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Floor.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Floor.element_dict:
            list_key = [key, Floor.element_dict[key]['weight'], Floor.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in Floor.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Floor.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Floor.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(Floor.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)

        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in Floor.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'floor_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Floor.c[0],
                               ge_headings=ge_headings, ge_data=ge, properties=properties,
                               property_list=property_list, elementlist=elementlist, reference=reference
                               )


@app.route('/Floor/<string:prop_na>')
def floor_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(Floor.c[1][prop_na]['elementlists'])
        reference = 'floor_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = Floor.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)


@app.route('/Floor/MaterialLayer/<string:ma>')
def floor_layer_structure(ma):
    headings = ('layer_id', 'material name', 'thickness[m]')
    data_info = Floor.c[3][ma]
    if isinstance(data_info, str):
        data= 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name'],value['thickness']))
    data = tuple(data)
    return render_template('materiallayer.html', data=data, headings=headings)


@app.route('/GroundFloor/')
def groundfloor():
    name = GroundFloor.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(GroundFloor.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in GroundFloor.element_dict:
            list_key = [key, GroundFloor.element_dict[key]['weight'], GroundFloor.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in GroundFloor.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(GroundFloor.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in GroundFloor.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(GroundFloor.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)

        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in GroundFloor.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'groundfloor_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=GroundFloor.c[0],
                               ge_headings=ge_headings, ge_data=ge, properties=properties,
                               property_list=property_list, elementlist=elementlist, reference=reference
                               )


@app.route('/GroundFloor/<string:prop_na>')
def groundfloor_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(GroundFloor.c[1][prop_na]['elementlists'])
        reference = 'groundfloor_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = GroundFloor.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)


@app.route('/GroundFloor/MaterialLayer/<string:ma>')
def groundfloor_layer_structure(ma):
    headings = ('layer_id', 'material name','thickness[m]')
    data_info = GroundFloor.c[3][ma]
    if isinstance(data_info, str):
        data= 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name'], value['thickness']))
    data = tuple(data)
    return render_template('materiallayer.html', data=data, headings=headings)


@app.route('/Window/')
def window():
    name = Window.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Window.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Window.element_dict:
            list_key = [key, Window.element_dict[key]['weight'], Window.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in Window.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Window.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Window.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(Window.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)

        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in Window.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'window_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Window.c[0],
                               ge_headings=ge_headings, ge_data=ge, properties=properties,
                               property_list=property_list, elementlist=elementlist, reference=reference
                               )


@app.route('/Window/<string:prop_na>')
def window_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(Window.c[1][prop_na]['elementlists'])
        reference = 'window_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = Window.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)


@app.route('/Window/MaterialLayer/<string:ma>')
def window_layer_structure(ma):
    headings = ('layer_id', 'material name', 'thickness')
    data_info = Window.c[3][ma]
    if isinstance(data_info, str):
        data= 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name'], value['thickness']))
    data = tuple(data)
    return render_template('materiallayer.html', data=data, headings=headings)


@app.route('/Door/')
def door():
    name = Door.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Door.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Door.element_dict:
            list_key = [key, Door.element_dict[key]['weight'], Door.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in Door.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Door.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Door.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(Door.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)
        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in Door.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'door_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Door.c[0],
                               properties=properties,
                               ge_headings=ge_headings, ge_data=ge, property_list=property_list, elementlist=elementlist,
                               reference=reference
                               )


@app.route('/Door/<string:prop_na>')
def door_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(Door.c[1][prop_na]['elementlists'])
        reference = 'door_layer_structure'
        if prop_na == 'Materiallayerconstruction':
            ma_list = Door.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list, reference=reference)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data, reference=reference)


@app.route('/Door/MaterialLayer/<string:ma>')
def door_layer_structure(ma):
    headings = ('layer_id', 'material name','thickness')
    data_info = Door.c[3][ma]
    if isinstance(data_info, str):
        data= 'no layer structure'
        return render_template('materiallayernotexisiting.html', data=data)
    else:
        data = []
        for key, value in data_info.items():
            data.append((key, value['material_name'], value['thickness']))
    data = tuple(data)
    return render_template('materiallayer.html', data=data, headings=headings)


@app.route('/ThermalZone/')
def thermalzone():
    name = ThermalZone.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(ThermalZone.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in ThermalZone.element_dict:
            list_key = [key, ThermalZone.element_dict[key]['weight'], ThermalZone.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in ThermalZone.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(ThermalZone.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in ThermalZone.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(ThermalZone.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)
        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in ThermalZone.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)

        reference = 'thermalzone_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=ThermalZone.c[0],
                               properties=properties, ge_headings=ge_headings, ge_data=ge,
                               property_list=property_list, elementlist=elementlist, reference=reference
                               )


@app.route('/ThermalZone/<string:prop_na>')
def thermalzone_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(ThermalZone.c[1][prop_na]['elementlists'])
        if prop_na == 'Materiallayerconstruction':
            ma_list = ThermalZone.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data)


@app.route('/Building/')
def building():
    name = Building.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Building.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Building.element_dict:
            list_key = [key, Building.element_dict[key]['weight'], Building.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in Building.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Building.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Building.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(Building.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)
        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in Building.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'building_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Building.c[0],
                               properties=properties,
                               ge_headings=ge_headings, ge_data=ge, property_list=property_list, elementlist=elementlist,
                               reference=reference
                               )


@app.route('/Building/<string:prop_na>')
def building_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(Building.c[1][prop_na]['elementlists'])
        if prop_na == 'Materiallayerconstruction':
            ma_list = Building.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data)


@app.route('/Storey/')
def storey():
    name = Storey.default
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Storey.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Storey.element_dict:
            list_key = [key, Storey.element_dict[key]['weight'], Storey.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        """get the detailed data of every element"""

        properties = ['elementname']
        for key in Storey.element_dict:
            if key != 'Materiallayerconstruction':
                properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Storey.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Storey.element_dict.keys():
                if key != 'Materiallayerconstruction':
                    each_elementlist.append(Storey.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)
        """get the geometry data of the elements"""
        ge_headings = ('Element Name', 'Global ID', 'Position', 'Orientation')
        ge = []
        for element in Storey.elements:
            list_ge = [element.Name, element.GlobalId, calc_position(element), calc_orientation(element)]
            ge.append(tuple(list_ge))
        ge = tuple(ge)
        reference = 'storey_prop_na'

        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Storey.c[0],
                               properties=properties,
                               ge_headings=ge_headings, ge_data=ge, property_list=property_list, elementlist=elementlist,
                               reference=reference
                               )


@app.route('/Storey/<string:prop_na>')
def storey_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        headings = 'elementlist'
        data = tuple(Storey.c[1][prop_na]['elementlists'])
        if prop_na == 'Materiallayerconstruction':
            ma_list = Storey.c[3].keys()
            ma_list = tuple(ma_list)
            return render_template('prop.html', title=title, headings=headings, ma_list=ma_list)
        else:
            return render_template('prop.html', title=title, headings=headings, data=data)


@app.route('/Material/')
def material():
    name = 'material'
    headings = ('Property', 'Weight', 'Score')
    data = []
    if len(Material.elements) == 0:
        data = 'The searched Entity is not existing in the IFC Model'
        return render_template('Entitynotexisting.html', data = data)
    else:
        for key in Material.element_dict:
            list_key = [key, Material.element_dict[key]['weight'], Material.element_dict[key]['score']]
            # print(list_key)
            data.append(tuple(list_key))
        properties = ['elementname']
        for key in Material.element_dict:
            properties.append(key)
        properties = tuple(properties)

        data = tuple(data)

        elements_name = list(Material.c[2].keys())
        elementlist = []
        # print(elements_name)
        for elem in range(len(elements_name)):
            each_elementlist = [elements_name[elem]]
            for key in Material.element_dict.keys():
                each_elementlist.append(Material.c[2][elements_name[elem]][key])
            elementlist.append(tuple(each_elementlist))
        elementlist = tuple(elementlist)
        reference = 'material_prop_na'
        return render_template('layout.html', name=name, headings=headings, data=data, final_score=Material.c[0],
                               properties=properties, property_list=property_list, elementlist=elementlist,
                               reference=reference)


@app.route('/Material/<string:prop_na>')
def material_prop_na(prop_na):
    if prop_na in property_list:
        title = prop_na
        data = tuple(Material.c[1][prop_na]['elementlists'])
        return render_template('prop.html', title=title, data=data)

if __name__ == '__main__':
    app.run(port=5000, debug=True)