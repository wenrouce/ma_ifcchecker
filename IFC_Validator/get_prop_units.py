# -*- coding:utf-8 -*-
'''get the unit of the properties need to be checked'''



from pint import UnitRegistry
from load_file import load_ifc
import numbers

ureg = UnitRegistry()
ifcunits = {}
ifc_pint_unitmap = {
    'AMPERE': 'ampere',
    'BECQUEREL': 'becquerel',
    'CANDELA': 'candela',
    'COULOMB': 'coulomb',
    'CUBIC_METRE': 'cubic metre',
    'DEGREE_CELSIUS': 'degree_Celsius',
    'FARAD': 'farad',
    'GRAM': 'gram',
    'GRAY': 'gray',
    'HENRY': 'henry',
    'HERTZ': 'hertz',
    'JOULE': 'joule',
    'KELVIN': 'kelvin',
    'LUMEN': 'lumen',
    'LUX': 'lux',
    'METRE': 'metre',
    'MOLE': 'mole',
    'NEWTON': 'newton',
    'OHM': 'ohm',
    'PASCAL': 'pascal',
    'RADIAN': 'radian',
    'SECOND': 'second',
    'SIEMENS': 'siemens',
    'SIEVERT': 'sievert',
    'SQUARE_METRE': 'square metre',
    'STERADIAN': 'steradian',
    'TESLA': 'tesla',
    'VOLT': 'volt',
    'WATT': 'watt',
    'WEBER': 'weber'
}


def parse_ifc(unit_entity):
    """
    parse the unit type
    :param unit_entity:
    :return:
    """
    unit_type = unit_entity.is_a()
    if unit_type == 'IfcDerivedUnit':
        unit = ureg.dimensionless
        for element in unit_entity.Elements:
            prefix_string = element.Unit.Prefix.lower() if element.Unit.Prefix else ''
            unit_part = ureg.parse_units('{}{}'.format(prefix_string, ifc_pint_unitmap[element.Unit.Name]))
            if element.Unit.Dimensions:
                unit_part = unit_part ** element.Dimensions
            unit = unit * unit_part ** element.Exponent
        return unit
    elif unit_type == 'IfcSIUnit':
        prefix_string = unit_entity.Prefix.lower() if unit_entity.Prefix else ''
        unit = ureg.parse_units('{}{}'.format(prefix_string, ifc_pint_unitmap[unit_entity.Name]))
        if unit_entity.Dimensions:
            unit = unit ** unit_entity.Dimensions
        return unit
    elif unit_type == 'IfcConversionBasedUnit':
        # TODO: Test with multiple components? test if unit_component ist no IFCSIUnit?!?! Conversion?! Seperate
        #  or use in units?!
        unit_component = unit_entity.ConversionFactor.UnitComponent
        prefix_string = unit_component.Prefix.lower() if unit_component.Prefix else ''
        unit = ureg.parse_units('{}{}'.format(prefix_string, ifc_pint_unitmap[unit_component.Name]))
        if unit_component.Dimensions:
            unit = unit ** unit_component.Dimensions
        return unit
    elif unit_type == 'IfcMonetaryUnit':
        # TODO: Need To Be Testet Currency in IFC = Currency in PINT?
        currency = unit_entity.Currency
        try:
            unit = ureg.parse_units(currency)
        except:
            unit = ureg.dimensionless
        return unit
    else:
        pass  # TODO: Implement


def get_ifcunits(ifc_unit):
    unit_assignment = ifc_unit.by_type('IfcUnitAssignment')
    global ifcunits
    results = {}

    for unit_entity in unit_assignment[0].Units:
        try:
            key = 'Ifc{}'.format(unit_entity.UnitType.capitalize().replace('unit', 'Measure'))
            pos_key = 'IfcPositive{}'.format(unit_entity.UnitType.capitalize().replace('unit', 'Measure'))
            unit = parse_ifc(unit_entity)
            results[key] = unit
            results[pos_key] = unit
        except:
            print("Failed to parse %s" % unit_entity)
    ifcunits = results
    return results


get_ifcunits(load_ifc())



def parse_quantity_unit(quantity):
    if quantity == 'IfcQuantityLength':
        unit = ifcunits.get('IfcLengthMeasure')
        return unit
    elif quantity == 'IfcQuantityArea':
        unit = ifcunits.get('IfcAreaMeasure')
        return unit
    elif quantity == 'IfcQuantityVolume':
        unit = ifcunits.get('IfcVolumeMeasure')
        return unit


def property2dict(propertyset):
    """Converts IfcPropertySet to python dict"""
    propertydict = {}
    if hasattr(propertyset, 'HasProperties'):
        for prop in propertyset.HasProperties:
            # print(prop)
            if hasattr(prop, 'Unit'):
                unit = parse_ifc(prop.Unit) if prop.Unit else None
            else:
                unit = None
            if prop.is_a() == 'IfcPropertySingleValue':
                if prop.NominalValue is not None:
                    if prop.NominalValue.is_a() == 'IfcThermalTransmittanceMeasure':
                        unit = ifcunits.get('IfcThermaltransmittanceMeasure')
                    else:
                        unit = ifcunits.get(prop.NominalValue.is_a()) if not unit else unit
                    if unit:
                        propertydict[prop.Name] = '%.2f' % prop.NominalValue.wrappedValue * unit
                    else:
                        if isinstance(prop.NominalValue.wrappedValue, numbers.Number):
                            if prop.NominalValue.wrappedValue is True or False:
                                propertydict[prop.Name] = prop.NominalValue.wrappedValue
                            else:
                                propertydict[prop.Name] = '%.2f' % prop.NominalValue.wrappedValue
                        else:
                            propertydict[prop.Name] = prop.NominalValue.wrappedValue

            elif prop.is_a() == 'IfcPropertyListValue':
                # TODO: Unit conversion
                values = []
                for value in prop.ListValues:
                    unit = ifcunits.get(value.is_a()) if not unit else unit
                    if unit:
                        values.append('%.2f' % value.wrappedValue * unit)
                    else:
                        values.append(value.wrappedValue)
                propertydict[prop.Name] = values
            elif prop.is_a() == 'IfcPropertyBoundedValue':
                # TODO: Unit conversion
                propertydict[prop.Name] = (prop, prop)
                raise NotImplementedError("Property of type '%s'" % prop.is_a())
            elif prop.is_a() == 'IfcPropertyEnumeratedValue':
                # TODO: Unit conversion
                values = []
                for value in prop.EnumerationValues:
                    unit = ifcunits.get(value.is_a()) if not unit else unit
                    if unit:
                        values.append('%.2f' % value.wrappedValue * unit)
                    else:
                        values.append(value.wrappedValue)
                propertydict[prop.Name] = values
            else:
                raise NotImplementedError("Property of type '%s'" % prop.is_a())
    elif hasattr(propertyset, 'Quantities'):
        for prop in propertyset.Quantities:
            # print('Quantities',prop.is_a())
            unit = parse_quantity_unit(prop.is_a())
            for attr, p_value in vars(prop).items():
                if attr.endswith('Value'):
                    if p_value is not None:
                        if unit:
                            propertydict[prop.Name] = '%.2f' % p_value * unit
                        else:
                            propertydict[prop.Name] = p_value
                        break
    elif hasattr(propertyset, 'Properties'):
        for prop in propertyset.Properties:
            unit = parse_ifc(prop.Unit) if prop.Unit else None
            if prop.is_a() == 'IfcPropertySingleValue':
                if prop.NominalValue is not None:
                    unit = ifcunits.get(prop.NominalValue.is_a()) if not unit else unit
                    if unit:
                        propertydict[prop.Name] = '%.2f' % prop.NominalValue.wrappedValue * unit
                    else:
                        propertydict[prop.Name] = prop.NominalValue.wrappedValue
    return propertydict


def get_property_sets(element):
    """Returns all PropertySets of element

    :param element: The element in which you want to search for the PropertySets
    :return: dict(of dicts)
    """
    # TODO: Unit conversion
    property_sets = {}
    if hasattr(element, 'IsTypedBy'):
        for properties in element.IsTypedBy:
            # print(properties)
            if hasattr(properties, 'RelatingType'):
                if properties.RelatingType.HasPropertySets is not None:
                    for defined in properties.RelatingType.HasPropertySets:
                            if defined.Name == 'Analytische Eigenschaften':
                                property_set_name = defined.Name
                                property_sets[property_set_name] = property2dict(defined)
    if hasattr(element, 'IsDefinedBy'):
        for defined in element.IsDefinedBy:
            if hasattr(defined, 'RelatingPropertyDefinition'):
                property_set_name = defined.RelatingPropertyDefinition.Name
                property_sets[property_set_name] = property2dict(defined.RelatingPropertyDefinition)
    elif hasattr(element, 'Material'):
        for defined in element.Material.HasProperties:
            property_set_name = defined.Name
            property_sets[property_set_name] = property2dict(defined)
    elif element.is_a('IfcMaterial'):
        if hasattr(element, 'HasProperties'):
            for defined in element.HasProperties:
                property_set_name = defined.Name
                property_sets[property_set_name] = property2dict(defined)
        elif hasattr(element, 'IfcMaterialProperties'):
            for defined in element.HasProperties:
                property_set_name = defined.Name
                property_sets[property_set_name] = property2dict(defined)
    return property_sets
