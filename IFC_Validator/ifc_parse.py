# -*- coding:utf-8 -*-
import bisect
import logging
import math

import ifcopenshell.util
import ifcopenshell.util.element
import numpy as np

from get_prop_units import get_property_sets
from load_file import load_json, load_ifc


#filename = upload().destination
ifc = load_ifc()
'''get logger and write it in a txt file '''

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
handler = logging.FileHandler("log.txt", 'w')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)

logger.addHandler(handler)
logger.addHandler(console)

# todo set the template finder for the module
"""find the right json file to compare"""


# todo write the get true north function

# def get_true_north(ifcElement):
#     """Find the true north in degree of this element, 0 °C means positive
#     X-axis. 45 °C Degree means middle between X- and Y-Axis"""
#     project = getProject(ifcElement)
#     try:
#         true_north = project.RepresentationContexts[0].TrueNorth.DirectionRatios
#     except AttributeError:
#         true_north = [0, 1]
#     angle_true_north = math.degrees(math.atan(true_north[0] / true_north[1]))
#     return angle_true_north

def get_predefined_type(ifcElement):
    """Returns the predefined type of the IFC element"""
    try:
        predefined_type = getattr(ifcElement, 'PredefinedType')
        if predefined_type == "NOTDEFINED":
            predefined_type = None
        # print('predefined_type {} is: {}'.format(ifcElement.Name, predefined_type))
        # print(predefined_type)
        return predefined_type
    except AttributeError:
        return None


def get_layers_ifc(element, Ifcentity):
    """
    return the material layer
    :param element:
    :param Ifcentity:
    :return:
    """
    if Ifcentity == 'Roof':
        relation = 'RelatingMaterial'
        print( "IsDecomposedBy", getifcattribute(element, "IsDecomposedBy"))
        if len(getifcattribute(element, "IsDecomposedBy") )!= 0:
            object = element.IsDecomposedBy[0].RelatedObjects[0]
        else:
            object = element
        assoc_list = getifcattribute(object, "HasAssociations")
        for assoc in assoc_list:
            association = getifcattribute(assoc, relation)
            if hasattr(association, 'MaterialLayers'):
                layer_list = association.MaterialLayers
                if layer_list is not None:
                    dict = list(layer_list)
                    return dict
            else:
                layer_list = association
                if layer_list is not None:
                    dict = list(layer_list)
                    return dict
    else:
        relation = 'RelatingMaterial'
        assoc_list = getifcattribute(element, "HasAssociations")
        for assoc in assoc_list:
            association = getifcattribute(assoc, relation)
            if association is not None:
                layer_list = None
                # ifc wall relation is 'ForLayerSet'
                if hasattr(association, 'ForLayerSet'):
                    layer_list = association.ForLayerSet.MaterialLayers
                elif hasattr(association, 'Materials'):
                    layer_list = association.Materials
                # ifc Window relation is 'MaterialConstituents'
                elif hasattr(association, 'MaterialConstituents'):
                    layer_list = association.MaterialConstituents
                    # print(layer_list)
                if layer_list is not None:
                    dict = list(layer_list)
                    return dict


def getifcattribute(ifcElement, attribute):
    """
    Fetches non-empty attributes (if they exist).
    """
    try:
        return getattr(ifcElement, attribute)
    except AttributeError:
        pass


def get_nested(data, *args):
    """to get the nested data from a dict"""
    if args and data:
        element = args[0]
        if element:
            prop_value = data.get(element)
            return prop_value if len(args) == 1 else get_nested(prop_value, *args[1:])

def propertyset2dict(propertyset):
    """
    return the quantity of every required propertyset(for material)
    """
    propertydict = {}
    if hasattr(propertyset, 'Properties'):
        for prop in propertyset.Properties:
            if prop.is_a() == 'IfcPropertySingleValue':
                if prop.NominalValue is not None:
                    propertydict[prop.Name] = prop.NominalValue.wrappedValue
            else:
                raise NotImplementedError("Property of type '%s'" % prop.is_a())
    return propertydict


def checker(instance, pro, val):
        """
            check if the quantity is missing or not, and set the logging report
        """
        if val is None:
            logger.info('{}:  the value of {} was missing.'.format(instance, pro))
        # elif val == 0:
        #     logger.info('{}: the value of {} was {}'.format(instance, pro, val))
        elif val:
            logger.info('{}: the value of {} was {}'.format(instance, pro, val))


def js_pset(element_name):
    """extract the json file"""
    js = load_json()
    jslist = []
    if element_name in js:
        b = js[element_name]['default_ps']
        for js_value in b.values():
            jslist.append(js_value[:])
    # logger.info(jslist)
    return jslist


def vector_angle(vector):
    """get the vector angle"""
    x = vector[0]
    y = vector[1]
    try:
        tang = math.degrees(math.atan(x / y))
    except ZeroDivisionError:
        if x > 0:
            return 90
        elif x < 0:
            return 270
        else:
            return 0
    if x >= 0:
        # quadrant 1
        if y > 0:
            return tang
        # quadrant 2
        else:
            return tang + 180
    else:
        # quadrant 3
        if y < 0:
            return tang + 180
        # quadrant 4
        else:
            return tang + 360


def angle_equivalent(angle):
    """ge the angel between 0 and 360"""
    while angle >= 360 or angle < 0:
        if angle >= 360:
            angle -= 360
        elif angle < 0:
            angle += 360
    return angle


def check(element, element_list, element_dic):
    """compare the json file with extracted ifc quantities"""
    """total_instance_Rate: to get the existing rate of every element"""
    elementlist = {}
    key_element_dic = []
    ma_di = {}

    for key in element_dic.keys():
        key_element_dic.append(key)
    json_name = js_pset(element)
    if element_list is None:
        """return score 0 if the element-list is none"""
        logger.warning('{} was not found'.format(element))
        weighted_avg = 0
    else:
        for j in range(len(json_name)):
            element_dic[key_element_dic[j]]['elementlists'] = []
            logger.info("Element {} is going to be checked".format(json_name[j]))
            total_value_every_quantity = 0
            """total_value_every_quantity: count the total number of every to be checked property"""
            count = 0
            """count: count the not none value"""
            if json_name[j][1] == "MaterialLayer":
                logger.info('checking the material layer...')

                for ele in element_list:
                    total_value_every_quantity += 1
                    d1 = ma_di.setdefault(ele.Name, {})
                    if not hasattr(ele, 'HasAssociations'):
                        element_dic[key_element_dic[j]]['elementlists'].append('{} associated relationship can not be '
                                                                               'found'.format(ele.Name))
                        logger.info('{}: the layer structure was missing '.format(ele.Name))
                    else:
                        if get_layers_ifc(ele, element) is not None:
                            count += 1
                            ele_layer = get_layers_ifc(ele, element)
                            for each_layer in range(len(ele_layer)):
                                d2 = d1.setdefault(each_layer + 1, {})
                                if hasattr(ele_layer[each_layer], 'Name'):
                                    d2.setdefault('material_name', ele_layer[each_layer].Name)
                                elif hasattr(ele_layer[each_layer], 'Material'):
                                    d2.setdefault('material_name', ele_layer[each_layer].Material.Name)
                                else:
                                    d2.setdefault('material_name', 'NoneValue')
                                if hasattr(ele_layer[each_layer], 'LayerThickness'):
                                    d2.setdefault('thickness', ele_layer[each_layer].LayerThickness)
                                else:
                                    d2.setdefault('thickness', 'NoneValue')
                            logger.info('{}: the layer structure was {} '.format(ele.Name, ma_di[ele.Name]))
                        else:
                            element_dic[key_element_dic[j]]['elementlists'].append('{} associated material '
                                                                                'construction cant be found'.format(
                                ele.Name))
                            ma_di[ele.Name] = 'no layer structure'
                            logger.info('{}: the layer structure was missing '.format(ele.Name))
            else:
                for m in range(0, len(element_list)):
                    every_psets = get_property_sets(element_list[m])
                    instance_val = get_nested(every_psets, json_name[j][0], json_name[j][1])
                    if json_name[j][1] == 'IsExternal':
                        if instance_val == '0.00':
                            instance_val = 'False'

                    elementlist.setdefault(element_list[m].Name, {})
                    elementlist[element_list[m].Name][key_element_dic[j]] = instance_val
                    total_value_every_quantity += 1
                    if instance_val is None:
                        element_dic[key_element_dic[j]]['elementlists'].append(element_list[m].Name)
                    if instance_val is not None or instance_val is False:
                        count += 1
                    checker(element_list[m].Name, json_name[j][1], instance_val)

            percent = count / total_value_every_quantity * 100
            element_dic[key_element_dic[j]]['score'] = percent

        values_element_dic = element_dic.values()
        weight_each_property = []
        score_each_property = []
        for each_element in values_element_dic:
            weight_each_property.append(each_element['weight'])
            score_each_property.append(each_element['score'])

        weighted = np.average(score_each_property, weights=weight_each_property)
        weighted_avg = round(weighted*100)/100
    check_value = [weighted_avg, element_dic, elementlist, ma_di]
    return check_value


def calc_position(element):
    """returns absolute position"""
    list_positions = {}
    placementrel = element.ObjectPlacement
    while placementrel is not None:
        if placementrel.RelativePlacement.Location is not None:
            rel_postition = np.array(placementrel.RelativePlacement.Location.Coordinates)
            list_positions[placementrel.PlacesObject[0].GlobalId] = rel_postition
        else:
            list_positions[placementrel.PlacesObject[0].GlobalId] = None
        placementrel = placementrel.PlacementRelTo

    pos_sum = 0
    for key in list_positions:
        pos = list_positions[key]
        if pos is not None:
            pos_sum += pos

    return pos_sum


def get_position(elementlist):
    pos_elements = {}
    for everyelement in elementlist:
        pos_elements[everyelement.Name] = calc_position(everyelement)
        logger.info('position for everyelements {} was {}'.format(everyelement.Name, pos_elements[everyelement.Name]))
    return pos_elements


def calc_orientation(element):
    list_angles = {}
    placementrel = element.ObjectPlacement

    while placementrel is not None:

        if placementrel.RelativePlacement.RefDirection is not None:
            o2 = placementrel.RelativePlacement.RefDirection.DirectionRatios
            list_angles[placementrel.PlacesObject[0].GlobalId] = vector_angle(o2)
        else:
            list_angles[placementrel.PlacesObject[0].GlobalId] = None
        placementrel = placementrel.PlacementRelTo
    ang_sum = 0

    for key in list_angles:
        ang = list_angles[key]
        if ang is not None:
            ang_sum += ang

    if ang_sum is None:
        return None
    # specific case windows
    if element.is_a('IfcWindow'):
        ang_sum += 180

    # angle between 0 and 360
    return angle_equivalent(ang_sum)


def get_orientation(elementlist):
    ang_elements = {}
    for everyelement in elementlist:
        ang_elements[everyelement.Name] = calc_orientation(everyelement)
        logger.info(
            'orientation for everyelements {} was {}'.format(everyelement.Name, ang_elements[everyelement.Name]))
    return ang_elements

"""assign importance factor to each element type, these values can be replaced later if needed"""
element_weight = {
    'OuterWall': [3],
    'InnerWall': [2],
    'Roof': [2],
    'Roof_ifcSlab': [2],
    'Floor': [2],
    'GroundFloor': [3],
    'Window': [3],
    'Door': [2],
    'ThermalZone': [3],
    'Building': [1],
    'Storey': [1],
    'Material': [5]
}


elements = ifc.by_type('IfcWall')
"""sort the external wall and internal wall"""
external_wall = []
internal_wall = []
for wall_ele in range(len(elements)):
    get_wall_boundaries = elements[wall_ele].ProvidesBoundaries
    if len(get_wall_boundaries) != 0:
        each_ori = []
        for boundary_ele in range(len(get_wall_boundaries)):
            get_wall_orientation = get_wall_boundaries[boundary_ele].InternalOrExternalBoundary
            each_ori.append(get_wall_orientation)
        if 'EXTERNAL' in each_ori:
            external_wall.append(elements[wall_ele])
        elif 'INTERNAL' in each_ori:
            internal_wall.append(elements[wall_ele])
    else:
        get_property = ifcopenshell.util.element.get_psets(elements[wall_ele])
        get_isexternal = get_nested(get_property, "Pset_WallCommon", "IsExternal")
        if get_isexternal is True:
            external_wall.append(elements[wall_ele])
        elif get_isexternal is False:
            internal_wall.append(elements[wall_ele])

try:
    if len(external_wall) + len(internal_wall) != len(elements):
        raise Exception('There exists missing wall instance')
except Exception as e:
    logger.warning(e)



class OuterWall:
    """check the properties of the element type OuterWall"""
    default = 'OuterWall'
    elements = external_wall
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
                 Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 0},
            "Length": {'weight': 2},
            "Width": {'weight': 2},
            "Height": {'weight': 2},
            "GrossFootprintArea": {'weight': 2},
            "NetFootprintArea": {'weight': 2},
            "GrossSideArea": {'weight': 2},
            "NetSideArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
            "GrossWeight": {'weight': 2},
            "NetWeight": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4},
            "*Area*": {'weight': 0},
            "*Tilt*": {'weight': 0},
            "*ThermalResistance R*": {'weight': 0},
            "*ThermalMass*": {'weight': 0}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        get_position(elements)
        get_orientation(elements)
        element_dict = c[1]


class InnerWall:
    """check the properties of the element type InnerWall"""
    default = 'InnerWall'
    elements = internal_wall
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
                    Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 0},
            "Length": {'weight': 2},
            "Width": {'weight': 2},
            "Height": {'weight': 2},
            "GrossFootprintArea": {'weight': 2},
            "NetFootprintArea": {'weight': 2},
            "GrossSideArea": {'weight': 2},
            "NetSideArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
            "GrossWeight": {'weight': 2},
            "NetWeight": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4},
            "*Area*": {'weight': 0},
            "*Tilt*": {'weight': 0},
            "*ThermalResistance R*": {'weight': 0},
            "*ThermalMass*": {'weight': 0}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        get_position(elements)
        get_orientation(elements)
        element_dict = c[1]

class Roof:
    elements = ifc.by_type('IfcRoof')
    if len(elements) != 0:
        """Roof elment type can be created by IfcRoof or IfcSlab"""
        default = 'Roof'
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 2},
            "GrossArea": {'weight': 2},
            "NetArea": {'weight': 2},
            "ProjectedArea": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4},
            "*Area*": {'weight': 0},
            "*Width*": {'weight': 0}
        }
    else:
        default = 'Roof_ifcSlab'
        elements = []
        slab = ifc.by_type('IfcSlab')
        for i in range(len(slab)):
            every_slab_instance = slab[i]
            type1 = get_predefined_type(every_slab_instance)
            if type1 is None:
                pass
            else:
                print(type1)
                if 'ROOF' in type1:
                    elements.append(every_slab_instance)
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 2},
            "Width": {'weight': 2},
            "Length": {'weight': 2},
            "Depth": {'weight': 2},
            "Perimeter": {'weight': 2},
            "GrossArea": {'weight': 2},
            "NetArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
            "GrossWeight": {'weight': 2},
            "NetWeight": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4},
            "*Area*": {'weight': 0},
            "*Width*": {'weight': 0}
        }
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
        Please check the ifc model again""".format(default))
    else:
        c = check(default, elements, dic_weight_property)
        del element_weight['Roof_ifcSlab']
        default ='Roof'
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        element_dict = c[1]

"""sort the groudfloor and floor"""
floor = []
ground_floor = []
slab = ifc.by_type('IfcSlab')
for i in range(len(slab)):
    every_slab_instance = slab[i]
    type1 = get_predefined_type(every_slab_instance)
    if type1 is None:
        pass
    else:
        if 'FLOOR' in type1:
            floor.append(every_slab_instance)
        elif 'BASESLAB' in type1:
            ground_floor.append(every_slab_instance)


class Floor:
    default = 'Floor'
    elements = floor
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
        Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 2},
            "Width": {'weight': 2},
            "Length": {'weight': 2},
            "Depth": {'weight': 2},
            "Perimeter": {'weight': 2},
            "GrossArea": {'weight': 2},
            "NetArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
            "GrossWeight": {'weight': 2},
            "NetWeight": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4},
            "*Area*": {'weight': 0},
            "*Width*": {'weight': 0}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        element_dict = c[1]


class GroundFloor:
    default = 'GroundFloor'
    elements = ground_floor
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
        Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 2},
            "Width": {'weight': 2},
            "Length": {'weight': 2},
            "Depth": {'weight': 2},
            "Perimeter": {'weight': 2},
            "GrossArea": {'weight': 2},
            "NetArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
            "GrossWeight": {'weight': 2},
            "NetWeight": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4},
            "*Area*": {'weight': 0},
            "*Width*": {'weight': 0}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        element_dict = c[1]


# #
#
class Window:
    default = 'Window'
    elements = ifc.by_type('IfcWindow')
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
        Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 2},
            "Infiltration": {'weight': 1},
            "Width": {'weight': 2},
            "Height": {'weight': 2},
            "Perimeter": {'weight': 2},
            "Area": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        get_position(elements)
        get_orientation(elements)
        element_dict = c[1]



class Door:
    default = 'Door'
    elements = ifc.by_type('IfcDoor')
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
        Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "ThermalTransmittance": {'weight': 3},
            "IsExternal": {'weight': 2},
            "Infiltration": {'weight': 1},
            "Width": {'weight': 2},
            "Height": {'weight': 2},
            "Perimeter": {'weight': 2},
            "Area": {'weight': 2},
            "Materiallayerconstruction": {'weight': 4}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        get_position(elements)
        get_orientation(elements)
        element_dict = c[1]



class ThermalZone:
    default = 'ThermalZone'
    elements = ifc.by_type('IfcSpace')
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
        Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "Reference": {'weight': 2},
            "IsExternal": {'weight': 2},
            "GrossPlannedArea": {'weight': 2},
            "NetPlannedArea": {'weight': 2},
            "OccupancyType": {'weight': 4},
            "OccupancyNumber": {'weight': 3},
            "OccupancyNumberPeak": {'weight': 2},
            "OccupancyTimePerDay": {'weight': 2},
            "AreaPerOccupant": {'weight': 2},
            "SpaceTemperatureMax": {'weight': 4},
            "SpaceTemperatureMin": {'weight': 4},
            "NaturalVentilationRate": {'weight': 2},
            "MechanicalVentilationRate": {'weight': 2},
            "People": {'weight': 2},
            "RelativeHumidity": {'weight': 2},
            "Height": {'weight': 2},
            "GrossPerimeter": {'weight': 2},
            "NetPerimeter": {'weight': 4},
            "GrossFloorArea": {'weight': 2},
            "NetFloorArea": {'weight': 2},
            "GrossWallArea": {'weight': 2},
            "NetWallArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
            "*Name*": {'weight': 0},
            "*Area*": {'weight': 0},
            "*Perimeter*": {'weight': 0},
            "*NetHeight*": {'weight': 0}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        element_dict = c[1]


class Building:
    default = 'Building'
    elements = ifc.by_type('IfcBuilding')
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
           Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "OccupancyType": {'weight': 1},
            "GrossPlannedArea": {'weight': 2},
            "NetPlannedArea": {'weight': 2},
            "NumberOfStoreys": {'weight': 2},
            "YearOfConstruction": {'weight': 4},
            "Height": {'weight': 2},
            "FootprintArea": {'weight': 2},
            "GrossFloorArea": {'weight': 2},
            "NetFloorArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        get_position(elements)
        get_orientation(elements)
        element_dict = c[1]

class Storey:
    default = 'Storey'
    elements = ifc.by_type('IfcBuildingStorey')
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
              Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "GrossPlannedArea": {'weight': 2},
            "NetPlannedArea": {'weight': 2},
            "GrossHeight": {'weight': 2},
            "NetHeight": {'weight': 2},
            "GrossPerimeter": {'weight': 2},
            "GrossFloorArea": {'weight': 2},
            "NetFloorArea": {'weight': 2},
            "GrossVolume": {'weight': 2},
            "NetVolume": {'weight': 2},
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        get_position(elements)
        get_orientation(elements)
        element_dict = c[1]


class Material:
    default = 'Material'
    elements = ifc.by_type('IfcMaterial')
    if len(elements) == 0:
        logger.warning("""there was no instance found for IfcElements {},
              Please check the ifc model again""".format(default))
    else:
        dic_weight_property = {
            "SpecificHeatCapacity": {'weight': 4},
            "MassDensity": {'weight': 4},
            "ThermalConductivity": {'weight': 4}
        }
        c = check(default, elements, dic_weight_property)
        element_weight[default].append(c[0])
        logger.info('{}: {}'.format(default, c[0]))
        element_dict = c[1]

"""get the final score of the checked ifc file"""
for value in element_weight.values():
    if len(value) == 1:
        value.append(0)
dic_value = list(element_weight.values())
weight_each_element = []
score_each_element = []
for each in dic_value:
    weight_each_element.append(each[0])
for each in dic_value:
    score_each_element.append(each[1])
final_score = round(np.average(score_each_element, weights=weight_each_element)*100)/100



def final_grade(rate, breakpoints=None, grades=None):
    if grades is None:
        grades = ['Befriedigend ', 'Bronze', 'Silber', 'Gold', 'Platin']
    if breakpoints is None:
        breakpoints = [35.00, 50.00, 65.00, 80.00]
    award = bisect.bisect(breakpoints, rate)
    return grades[award]


logger.info('The final grade of the ifc-data was {}'.format(final_score))
logger.info('The award of the ifc-data was {}'.format(final_grade(final_score)))

