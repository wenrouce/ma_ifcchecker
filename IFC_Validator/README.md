# Introduction of Section IFC_Validator

## Steps to validate ifc files
1. Define the to be checked IFC files

Open file **load_file.py**, line 9 should be modified as path of the to be checked ifc model

2. Define json template

Open file **load_file.py**, line 16 should be modified as path of the json template correspondent to the version of source tool.  **template_Autodesk Revit 2021 (ENU).json**, for example, is the json template file of Revit 2021 in english. 

3. Run **ifc_parse.py**

Run script  **ifc_parse.py**. A log file **log.txt** will be created after running. 

4. Get the web report by **app.py**
cd terminal to IFC_Validator folder input 'flask run' and then click the link in terminal.
