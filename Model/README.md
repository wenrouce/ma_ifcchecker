# Model Section Description
 The Model Section contains in total 3 models, below are the descirptions of each model.


## Revit model
### ERC_main_building.rte
It is the revit version of ERC Main Building 
## IFC models
### IFC_ERC_main_building.ifc
It is the final ifc version of ERC Main Buidling **after** the material enrichment with the help of  **IFC_Material_Enrichment_tool** section. The detailed enrichment process is also introduced in section IFC_Material_Enrichment_tool.

### IFC_bef_material_enrichment.ifc
It is the ifc version of ERC Main Building **before** the material enrichment.



