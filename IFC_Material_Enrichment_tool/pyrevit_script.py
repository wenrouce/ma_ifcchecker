# -*- coding:utf-8 -*-
"""return the thermal properties of materials in the model"""


__title__ = 'Thermal\nProperties'

import json
from Autodesk.Revit.DB import FilteredElementCollector, BuiltInCategory, Material, ElementId, DisplayUnitType,ThermalMaterialType, UnitUtils


doc = __revit__.ActiveUIDocument.Document

# uniapp = ExternalCommandData.Application()
# uidoc = uiapp.ActiveUIDocument
# doc = uidoc.Document;
# name = doc.Title;
# path = doc.PathName;


doc = __revit__.ActiveUIDocument.Document
mas = FilteredElementCollector(doc).OfCategory(BuiltInCategory.OST_Materials)
ex_fm = {}
for ma in mas:
    thermalAssetId = ma.ThermalAssetId
    if thermalAssetId != ElementId.InvalidElementId:
        pse = doc.GetElement(thermalAssetId)
        if pse is not None:
            asset = pse.GetThermalAsset()
            if asset.ThermalMaterialType == ThermalMaterialType.Solid:
                name = ma.Name
                o_conductivity = asset.ThermalConductivity
                conductivity = UnitUtils.ConvertFromInternalUnits(o_conductivity, DisplayUnitType.DUT_WATTS_PER_METER_KELVIN)
                o_density = asset.Density
                density = UnitUtils.ConvertFromInternalUnits(o_density, DisplayUnitType.DUT_KILOGRAMS_PER_CUBIC_METER)
                porosity = asset.Porosity
                o_specific_heat = asset.SpecificHeat
                specific_heat = UnitUtils.ConvertFromInternalUnits(o_specific_heat, DisplayUnitType.DUT_JOULES_PER_GRAM_CELSIUS)
                ex_fm[name] = {'ThermalConductivity': conductivity, 'MassDensity': density, 'SpecificHeatCapacity': specific_heat}


# json_ex_fm = json.dumps(ex_fm)
# print(json_ex_fm)
print(ex_fm)
doc_name = doc.Title
write_to_file = open ('C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity checker\\IFC_Material_Enrichment_tool\\%s.json'%doc_name, 'w')
write_to_file.write(json.dumps(ex_fm, ensure_ascii=False, indent = 4).encode('utf8'))

write_to_file.close()