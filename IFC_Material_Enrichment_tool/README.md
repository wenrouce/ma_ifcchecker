# Section IFC_Material_Enrichment_tool

## Function: 
Enrich the material information to from Revit exported IFC Model because originally Revit (2021) is not able to export the material properties which are very import for BEPS. 
## Input file:
+ ERC Main Building ifc version before material enrichment is the input file, also see in section **Model** _IFC_bef_material_enrichment.ifc_. 

+ File **Revit_To_IFC Configuration.json** can be uploaded to Revit as template settings to export IFC models. 
    + Please note the line 25 should be modified before use.
        + Line 25: **Revit_to_IFC_MappingFile.txt** in the section is an example file. The content of the file can be modified as needed.
 
## Enrichment Introduction
1. Install Pyrevit plugins in Revit 
Follow Youtube tutories (link: https://youtu.be/WJgmFkGHVPU) to integrate the **pyrevit_script.py** (Line 45, 46 need to be modified) as plugins in Revit.
2. Export a material list from Revit
Click the plugins button, a material list with material properties will be exported. **revit_material_list.json** in this folder is an example of exported list.
3. Import the material list into the original ifc model
    + In Pycharm run script **write_to_ifc_tool.py** 
        + Line 10 should be modified to the ifc path, which is before enrichment.
        + Line 16 should be modified to the from revit exported material list path.
    + An IFC model with material properties is avaliable now.


    

