# -*- coding:utf-8 -*-
# -*- coding:utf-8 -*-
# todo automatically add the material properties for ifc2*3 and ifc4 file
import json
import ifcopenshell

#ifc23_path = 'C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity checker\\example file\\trail with base quantites.ifc'
# ifc4_path = 'C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity checker\\example file\\Eonc.26.08_createSpaceRatherThanRoom.ifc'
'''please input the filepath of the ifc file before material enrichment'''
ifc4_path = 'C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity checker\\Model\\IFC_bef_material_enrichment.ifc'

ifc_path = ifc4_path
ifc_file = ifcopenshell.open(ifc_path)

'''please input the filepath of the material list exported from revit'''
revit_ma_json = open('C:\\Users\\Wenro\\PycharmProjects\\ifc quaiity '
                     'checker\\IFC_Material_Enrichment_tool\\revit_material_list.json', encoding='utf-8')

revit_ma = json.loads(revit_ma_json.read())


def write_ifc4_file(file, ifc_file):
    element = ifc_file.by_type('IfcMaterial')
    if element is not None:
        ma_list = []
        for ele in element:
            ma_name = ele.Name
            if ma_name in file:
                "get the specificheatcapacity value"
                s_h = file[ele.Name]["SpecificHeatCapacity"]
                s_h_new_prop = ifc_file.createIfcPropertySingleValue('SpecificHeatCapacity', None,
                                                                     ifc_file.create_entity("IfcReal", s_h), None)
                "get the ThermalConductivity value"
                t_h = file[ele.Name]["ThermalConductivity"]
                t_h_new_prop = ifc_file.createIfcPropertySingleValue('ThermalConductivity', None,
                                                                     ifc_file.create_entity("IfcReal", t_h), None)
                propertylist = [s_h_new_prop, t_h_new_prop]
                new_property = tuple(propertylist)
                ifc_file.createIfcMaterialProperties('Pset_MaterialThermal', None, new_property, ele)

                "get the MassDensity value"
                m_d = file[ele.Name]["MassDensity"]
                m_d_new_prop = ifc_file.createIfcPropertySingleValue('MassDensity', None,
                                                                     ifc_file.create_entity("IfcReal", m_d), None)
                ifc_file.createIfcMaterialProperties('Pset_MaterialCommon', None, [m_d_new_prop], ele)

                # ge = get_prop_units.get_property_sets(ele)
                # print(ge)
            else:
                ma_list.append(ma_name)
        print('there exists no thermal properties for material {}'.format(ma_list))

'''define the new created ifc filepath after material enrichment'''
modified_ifc_path = ifc_path[:-4] + '_modified.ifc'

write_ifc4_file(revit_ma, ifc_file)
ifc_file.write(modified_ifc_path)
# schema = ifc_file.schema
# print(schema)
# if schema == 'IFC4':
#     print('1')
#     write_ifc4_file(revit_ma, ifc_file)
# elif schema == 'IFC2X3':
#     print('2')
# else:
#     print('no value for ifc schema')


# proj2 = ifc.by_type('IfcWindow')[0]
# ge = proj2.IsDefinedBy[1].RelatingPropertyDefinition.HasProperties
# ae = get_prop_units.get_property_sets(proj2)
# print(ge, ae)
